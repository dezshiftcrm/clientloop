/* 
* Name          : Shift_Discharge_Loan_Extension
* Author        : Shift CRM
* Description   : Extension class for Discharge_Loan VF page
* Sharing Rules	: With sharing modifier used to run as User
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 01/26/2015         Desmond     1.0         Initial
* 
*/


public with sharing class Shift_Discharge_Loan_Extension {

    public Loan__c theLoan;

    public List<Loan_Payment__c> scheduledPaymentList;

    //Output fields
    public Decimal principalOwed {get; set;}
    public Decimal interestOwed {get; set;}
    public Decimal feesOwed {get; set;}
    public Decimal fullAmountOwed {get; set;}
    //public Decimal amountPaid {get; set;}
    public Decimal avaiableCashBalance {get; set;}
    public String businessName {get; set;}

    /*
    * Constructor
    */
    public Shift_Discharge_Loan_Extension(ApexPages.StandardController sc) {

        //theLoan = [SELECT Id, FROM Loan__c WHERE Id = 'a09F000000VcSjx'];
        //Pull all remaining Loan Payments schduled for after today and recordtype value
        theLoan = (Loan__c)sc.getRecord();
        theLoan = [SELECT Id, Outstanding_Principal__c, Total_Owed_by_Borrower__c, Business_Name__r.Name, Business_Name__r.Available_Cash_Balance__c, Interest_Rate__c, Loan_Start_Date__c FROM Loan__c WHERE Id = :theLoan.Id];

        RecordType scheduledPaymentRecordType =  [SELECT Id FROM RecordType WHERE DeveloperName = :Shift_Utility.SCHEDULED_PAYMENT AND SobjectType = :Shift_Utility.LOAN_PAYMENT_SOBJECT];
        scheduledPaymentList = [SELECT Id, Principal_Owed__c, Interest_Owed__c, Payment_Number_Sequence__c, Due_Date_in_Past__c, Payment_Due_Date__c, Fees_Owed__c FROM Loan_Payment__c lp WHERE Loan__c = :theLoan.Id AND RecordTypeId = :scheduledPaymentRecordType.Id ORDER By Payment_Number_Sequence__c ];
        calculateAmountsOwed();

    }

    /*
    * Calculate field values
    */
    public void calculateAmountsOwed (){

        //Used to caculate Interest owed and fees owed
        Loan_Payment__c lastLoanPaymentMade = null;
        
        Date lastPaymentDate;
        Integer timePeriod;
        
        Decimal interestCarriedOver = 0;
        feesOwed = 0;

        for (Loan_Payment__c tempLoanPayment : scheduledPaymentList){          

            if ( !tempLoanPayment.Due_Date_in_Past__c ){
                break;
            }

            lastLoanPaymentMade = tempLoanPayment;
            interestCarriedOver = tempLoanPayment.Interest_Owed__c;
            feesOwed = tempLoanPayment.Fees_Owed__c;

        }

        // if lastPayment is null no payments have been made yet, so set the loan start date as the Loan's start date
        if (lastLoanPaymentMade == null){
            lastPaymentDate = theLoan.Loan_Start_Date__c;
        } else {
            lastPaymentDate = lastLoanPaymentMade.Payment_Due_Date__c;
        }
        
        //Caclulate number of days since last payment
        timePeriod = lastPaymentDate.daysBetween(System.today());

		principalOwed = (theLoan.Outstanding_Principal__c == null) ? 0 : theLoan.Outstanding_Principal__c;
        interestOwed = interestCarriedOver + (principalOwed * theLoan.Interest_Rate__c /365 * timePeriod / 100);
        avaiableCashBalance = theLoan.Business_Name__r.Available_Cash_Balance__c;
        businessName = theLoan.Business_Name__r.Name;
        fullAmountOwed = principalOwed + interestOwed + feesOwed;

        //formatting to 2 decimals
		principalOwed = principalOwed.setScale(2);
		interestOwed = interestOwed.setScale(2);
		fullAmountOwed = fullAmountOwed.setScale(2);
    }

    /*
    * Deletes the unpaid payments, and creates discharge payment.
    */ 
    public PageReference save(){

    	if (theLoan.Business_Name__r.Available_Cash_Balance__c < fullAmountOwed){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, System.label.Insufficient_Funds));
            return null;
        }

        Shift_Discharge_Loan.dischargeLoan(theLoan, principalOwed, interestOwed, feesOwed);
        
        //Redirect back to loan page
        PageReference lp = new PageReference('/'+ theLoan.Id);
        lp.setRedirect(true);
        return lp;
    }

    /*
    * Cancel action
    */
    public PageReference cancel(){
        //Redirect back to loan page
        PageReference lp = new PageReference('/'+ theLoan.Id);
        lp.setRedirect(true);
        return lp;
    }

}