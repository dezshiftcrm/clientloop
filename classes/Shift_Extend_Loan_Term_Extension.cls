/* 
* Name          : Shift_Extend_Loan_Term_Extension
* Author        : Shift CRM
* Description   : Extension for VF page Extend_Loan_Term
* Sharing Rules : With sharing modifier used to run as System for Visualforce page
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 01/20/2015         Desmond     1.0          Initial
*/

public with sharing class Shift_Extend_Loan_Term_Extension{

    public Loan__c theLoan;
    public Integer newLoanTerm {get; set;}
    public Integer newAmoritizationPeriod {get; set;}

    /*
    * Constructor
    */ 
    public Shift_Extend_Loan_Term_Extension(ApexPages.StandardController sc) {
        theLoan = (Loan__c)sc.getRecord();
        //theLoan = [SELECT Id, Initial_Loan_Term__c, Loan_Term__c, Amortization_Period__c FROM Loan__c WHERE Id = :theLoan.Id];
    }

    /*
    * Calls function extendTerm from Shift_ExtendLoanTerm class to increase the loan term value
    */ 
    public PageReference save(){
        
        if (newAmoritizationPeriod == null || newAmoritizationPeriod == 0){
            newAmoritizationPeriod = Integer.valueOf(theLoan.Amortization_Period__c);
        }

        //No loan term provided
        if ( newLoanTerm == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter value'));
            return null;
        //Loan Term is less than previous value
        } else if ( newLoanTerm <= theLoan.Loan_Term__c){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The new loan term must be greater than the current'));
            return null;
        }

        if (newAmoritizationPeriod == null){
            newAmoritizationPeriod = Integer.valueOf(theLoan.Amortization_Period__c);
        }

        if (newAmoritizationPeriod < newLoanTerm){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.Loan_Term_and_Period));
            return null;
        }

        //Call Extend Loan Term function from Extend Loan Term class
        Shift_ExtendLoanTerm.extendTerm(theLoan, Integer.valueOf(newLoanTerm), Integer.valueOf(newAmoritizationPeriod));
        
        //Redirect back to loan page
        PageReference lp = new PageReference('/'+ theLoan.Id);
        lp.setRedirect(true);
        return lp;
    }

    /*
    * Cancel all changes and return to the current loan.
    */ 
    public PageReference cancel(){
        //Redirect back to loan page
        PageReference lp = new PageReference('/'+ theLoan.Id);
        lp.setRedirect(true);
        return lp;
    }

}