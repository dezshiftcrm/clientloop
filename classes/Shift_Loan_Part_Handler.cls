/* 
 * Name             : Shift_Loan_Part_Handler
 * Author           : Shift CRM
 * Description      : Manages firing of triggered events on Loan_Part__c
 * Sharing Rules    : Without sharing modifier used to run as System
 *
 * Maintenance History: 
 * Date ------------  Name  ----  Version ---  Remarks 
 * 12/08/2014         Desmond     1.0          Initial
 */

public without sharing class Shift_Loan_Part_Handler {

    /*
     * Trigger handler for Loan Part records on after insert - When a Loan is fully funded and the stage is Listed Loan
     * Update the Stage to Funded - Pending Closure, the duration of listing and the record type of the Loan
     */
    @future
    public static void onAfterInsert(){

        Loan__c[] loanList = [SELECT Id, RecordTypeId, Listing_Begins__c, Stage__c FROM Loan__c WHERE Stage__c = :Shift_Utility.LISTED_LOAN AND Funded__c = 100];

        if (loanList.isEmpty()){
            System.debug('Dez return');
            return;
        }
        
        RecordType rt = Shift_Utility.getRecordType(Shift_Utility.FUNDED_AWAITING_DEVELOPER_NAME);
        for (Loan__c l : loanList){
            //Calculate days between
            Decimal dayDiff = Decimal.valueOf(((System.now()).getTime() - (l.Listing_Begins__c).getTime())/ (1000*60*60*24));
            dayDiff = dayDiff.setScale(8);

            //Set Fields
            l.RecordTypeId = rt.Id;
            l.Stage__c = Shift_Utility.FUNDED_AWAITING_ACCEPTANCE;
            l.Duration_of_Listing__c = dayDiff;
        }

        //Shift_Loan_Handler.updateLoanList(loanList);
        Database.update(loanList);
    }

    /*
     * When a Loan's stage becomes changes update all listed loan Parts to reflect the stage change
     */ 
    public static void updateLoanPartStatus (List<Id> loanIdList, String status){

        //Only process the given list if it is not empty
        if(loanIdList.isEmpty()) {
            return;
        }

        List<Loan_Part__c> loanPartList = [SELECT Id, Status__c, Loan__c FROM Loan_Part__c WHERE Loan__c IN :loanIdList];

        for (Loan_Part__c currentLoanPart : loanPartList){
            //Update each status for the Loan Parts
            currentLoanPart.Status__c = status;
        }

        //Update loan parts
        Database.SaveResult[] result = Database.Update(loanPartList, false);
        Handle_Errors_LoanParts(result, loanPartList);
    }

    /*
     * Method for handling insertion/update errors
     */
    @TestVisible 
    public static void Handle_Errors_LoanParts(Database.SaveResult[] result, list<Loan_Part__c> resultList){
        //Counter to loop through list
        Integer counter = 0;

        for (Database.SaveResult sr : result) {
            if (sr.isSuccess()) {
                //Operation was successful, so get the ID of the record that was processed
                System.debug('#### Update/Insert Operation Successful. Loan_Part__c ID: ' + sr.getId());
            } else {
                //Operation failed, so get all errors
                for(Database.Error err : sr.getErrors()) {
                    System.debug('#### The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('#### Update/Insert Operation fields that affected this error: ' + err.getFields());
                }
                trigger.newmap.get(resultList[counter].Loan__c).adderror( sr.getErrors()[0].getmessage() + ' | '+ resultList[counter].id);  
                resultList[counter].adderror( sr.getErrors()[0].getmessage() + ' | '+ resultList[counter].Id);                
            }
            counter++;  
        }
    }
}