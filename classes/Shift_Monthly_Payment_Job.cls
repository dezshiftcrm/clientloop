/* 
* Name           : Shift_Monthly_Payment_Job 
* Author         : Shift CRM
* Description    : Schedulable process for automating
*                   - If a Loan Payment's due date is today and the borrower has sufficient funds pay it off
*                   - If the borrower has insufficient funds continue to check everyday for sufficient funds and then pay it off
*                   - If the borrower does not have sufficinet funds by the end of the grace period, make a partial payment
* Sharing Rules : Without sharing modifier used to run as System
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 01/21/2014         Desmond     1.0         Initial
* 
*/


global class Shift_Monthly_Payment_Job implements Schedulable {

    global List<Loan_Payment__c> loanPaymentList;
    

    global Shift_Monthly_Payment_Job (){
    
        //Caculate Grace period
        Date today = Date.today();
        Date graceDate = today.addDays(Integer.valueOf(Shift_Payment_Config__c.getOrgDefaults().Grace_Period__c));

        loanPaymentList = [SELECT Id, Loan__r.Stage__c, Total_Owed__c, Total_Paid__c, Fees_Owed__c, Fees_Paid__c, Interest_Owed__c, Interest_Paid__c, Interest_Scheduled__c, Loan__c, Principal_Owed__c, Principal_Paid__c, Payment_Due_Date__c  FROM Loan_Payment__c where Payment_Due_Date__c >= TODAY AND Payment_Due_Date__c < :graceDate AND Loan__r.Stage__c = 'Closed' AND Payment_Status__c IN ('Scheduled', 'Unpaid')];
    }

    global void execute(SchedulableContext ctx){

        Account currentAccount;
        Loan_Payment__c[] updateList = new List<Loan_Payment__c>();

        //Get Corresponding Loans
        Set<Id> loanIdSet = getLoanIdSet(loanPaymentList);
        Loan__c[] loanList = [SELECT Id, Business_Name__c FROM Loan__c WHERE Id IN :loanIdSet];
        Map<Id, Loan__c> loanMap = new Map<Id, Loan__c>(loanList);

        //Get Accounts
        Set<Id> accountIdSet = getAccountIdSet(loanList);
        Account[] accountList = [SELECT Id, Available_Cash_Balance__c FROM Account WHERE Id IN : accountIdSet];
        Map<Id, Account> accountMap = new Map<Id, Account>(accountList);

        //Caculate Grace period
        Date today = Date.today();
        Date graceDate = today.addDays(Integer.valueOf(Shift_Payment_Config__c.getOrgDefaults().Grace_Period__c));

        for (Loan_Payment__c lp : loanPaymentList){

            currentAccount = accountMap.get( (loanMap.get(lp.loan__c)).Business_Name__c );

            //Insufficient Funds
            if (lp.Total_Owed__c > currentAccount.Available_Cash_Balance__c){

                //If Payment_Due_Date__c is or past the grace period and insufficient funds, do a partial payment
                if (lp.Payment_Due_Date__c == graceDate){

                    //True - allow partial payment
                    updateList.add( payLoanPayment(lp, lp.Total_Owed__c, true));
                }

            //Sufficient Funds
            } else {

                //False - No partial payment
                updateList.add( payLoanPayment(lp, lp.Total_Owed__c, false));
            }
        }

        //Update the given list
        Shift_Loan_Payment_Handler.updateLoanPaymentList(updateList);
    }

    /*
    * Boolean - partialPayment allows partial repayment to the loan, otherwise do nothing
    * Payments to a Loan Payment must be diverted in a specific order, fees, interest and then principal
    */
    private static Loan_Payment__c payLoanPayment (Loan_Payment__c loanPayment, Decimal amountPaid, Boolean partialPayment){

        if (amountPaid <= 0){
            return loanPayment;
        }

        Decimal remainingAmount = amountPaid;

        //Pay full amount to Fees if sufficient funds
        if (loanPayment.Fees_Owed__c <= remainingAmount){
            loanPayment.Fees_Paid__c = loanPayment.Fees_Owed__c;
            remainingAmount -= loanPayment.Fees_Owed__c;
        } else if (partialPayment){
            loanPayment.Fees_Paid__c = remainingAmount;
            return loanPayment;
        }

        //Pay full amount to Interest if sufficient funds
        if (loanPayment.Interest_Owed__c <= remainingAmount){
            loanPayment.Interest_Paid__c = loanPayment.Interest_Owed__c;
            remainingAmount -= loanPayment.Interest_Owed__c;
        } else if (partialPayment){
            loanPayment.Interest_Paid__c = remainingAmount;
            return loanPayment;
        }

        //Pay full amount to Principal if sufficient funds
        if (loanPayment.Principal_Owed__c <= remainingAmount){
            loanPayment.Principal_Paid__c = loanPayment.Principal_Owed__c;
            remainingAmount -= loanPayment.Principal_Owed__c;
        } else if (partialPayment){
            loanPayment.Principal_Paid__c = remainingAmount;
            return loanPayment;
        }       

        return loanPayment;

    }

    /*
    * Id Set Builder for Loans
    */
    private static Set<Id> getLoanIdSet(Loan_Payment__c[] loanPaymentList){
        Set<Id> returnList = new Set<Id>();

        for (Loan_Payment__c lp : loanPaymentList){
            returnList.add(lp.Loan__c);
        }

        return returnList;
    }

    /*
    * Id Set Builder for Accounts
    */
    private static Set<Id> getAccountIdSet(Loan__c[] loanList){
        Set<Id> returnList = new Set<Id>();

        for (loan__c l : loanList){
            returnList.add(l.Business_Name__c);
        }

        return returnList;
    }

}