/* 
* Name          : Shift_Expire_Listed_Loans_Schedule_Job
* Author        : Shift CRM
* Description   : Schedulable class to update Loans in stage 'Pending Listing', when Listing_Begins__c 
*				  greater than or equal today, then List the Loan.
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 01/22/2015         Desmond     1.0          Initial
*/

global class Shift_Expire_Listed_Loans_Schedule_Job implements Schedulable{
	
	global List<Loan__c> loanList;

	global Shift_Expire_Listed_Loans_Schedule_Job() {
		
		DateTime dateToday = System.now();

		//Query for Loans where stage is 'Listed Loan' and Listing ends is NOW
		loanList = [SELECT Id, RecordTypeId, Stage__c FROM Loan__c WHERE Stage__c = :Shift_Utility.LISTED_LOAN AND Listing_Ends__c >= :dateToday ];

	}

	global void execute (SchedulableContext ctx){

		//Get Pending Closure Record Type for Loan__c
		RecordType rt = [SELECT Id, name, developerName FROM RecordType WHERE DeveloperName = :Shift_Utility.DEAD_LOAN_DEVELOPER_NAME AND sObjectType = :Shift_Utility.LOAN_SOBJECT];

		//Set field values
		for (Loan__c l : loanList){

			//Set Fields
			l.RecordTypeId = rt.Id;
			l.Stage__c = 'Dead Loan';
			l.Dead_Loan_Reason__c = 'Not Funded During Listing';
		}

		Database.update(loanList);
	}
}