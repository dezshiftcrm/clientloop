/* 
* Name          : Shift_List_Loans_Schedule_Job
* Author        : Shift CRM
* Description   : Schedulable class to update Loans in stage 'Pending Listing', when Listing_Begins__c 
*				  greater than or equal today, then List the Loan.
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 01/22/2015         Desmond     1.0          Initial
*/

global class Shift_List_Loans_Schedule_Job implements Schedulable{
	
	global List<Loan__c> loanList;
	
	global Shift_List_Loans_Schedule_Job() {
		
		loanList = [SELECT Id, RecordTypeId, Stage__c FROM Loan__c WHERE Stage__c = 'Pending Listing' AND Listing_Begins__c <= TODAY ];
	
	}

	global void execute(SchedulableContext ctx){

		RecordType rt = [SELECT Id, name, developerName FROM RecordType WHERE DeveloperName = :Shift_Utility.LISTING_LOAN_DEVELOPER_NAME AND sObjectType = :Shift_Utility.LOAN_SOBJECT];

		for (Loan__c l : loanList){

			l.RecordTypeId = rt.Id;
			l.Stage__c = 'Listed Loan';
		}

		Database.update(loanList);
	}
}