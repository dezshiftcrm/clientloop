/* 
* Name          : Shift_Loan_Handler
* Author        : Shift CRM
* Description   : Handler functionality for Loan Trigger
* Sharing Rules	: Without sharing modifier used to run as System
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 12/03/2014         Desmond     1.0          Initial
* 06/02/2015         Desmond     1.1          Code Clean up
*/

public Without sharing class Shift_Loan_Handler {

	//Recursive Flag
	private static boolean run = true;
    
    /*
	* Recursive Method Check
	*/ 
	public static boolean runOnce(){
	    if (run){
	    	run = false;
	    	return true;
	    } else {
	        return run;
	    }
    }

	/*
	* AfterInsert
	*/ 
	public static void onAfterUpdate (Loan__c[] newLoans, Map<Id, loan__c> oldLoans){

		//Recusion check
		if (!runOnce()){
			return;
		}

		//Holder lists for processing loans from trigger		
		List<Loan_Payment__c> loanPaymentList = new list<Loan_Payment__c>();
		List<Id> oldLoanPaymentIdList = new list<Id>();
		List<Loan__c> generateTransactionList = new List<Loan__c>();

		//Lists for managing Loan stage updates, roll down to Loan Parts - maybe refactor Loan_part functionality to onBeforeUpdate to clean up code
		List<Id> loanDeadIdList = new list<Id>();
		List<Id> loanActiveIdList = new list<Id>();
		List<Id> loanInCollectionsIdList = new list<Id>();
		List<Id> loanRetiredIdList = new list<Id>();

		//Handle multiple loans for trigger
		for (Loan__c newLoan : newLoans){
			//Pull old loan to check for stage changes
			Loan__c oldLoan = oldLoans.get(newLoan.Id);
			System.debug('Old Loan Stage: ' + oldLoan.stage__c + ' new Loan Stage: ' + newLoan.Stage__c);

			//When the stage has been updated run the appropriate function
			if (oldLoan.stage__c != newLoan.stage__c) {
				//If Stage is Closed - Add them to lists for generating Loan Payments and Transactions
				if (newLoan.stage__c == Shift_Utility.CLOSED){
					//Add new Loan Payments to generation List
					loanPaymentList.addAll( Shift_Loan_Payment_Handler.generateMonthlyPaymentLoans(newLoan, false) );

					//Add Loans to Transaction generation List
					generateTransactionList.add(newLoan);

					//Add old Loan Payment records to be processed for deletion later
					oldLoanPaymentIdList.add(newLoan.Id);
				} else if (newLoan.stage__c == Shift_Utility.APPROVED_FINALIZING_DETAILS){
					loanPaymentList.addAll( Shift_Loan_Payment_Handler.generateMonthlyPaymentLoans(newLoan, true) );
					oldLoanPaymentIdList.add(newLoan.Id);
				} else if (newLoan.stage__c == Shift_Utility.DEAD_LOAN){
					loanDeadIdList.add(newLoan.Id);
				} else if (newLoan.stage__c == Shift_Utility.IN_COLLECTIONS){
					loanInCollectionsIdList.add(newLoan.Id);
				} else if (newLoan.stage__c == Shift_Utility.RETIRED_LOAN){
					loanRetiredIdList.add(newLoan.Id);
				}
			} else {
				run = true;
			}
		}

		//Delete the old Loan Payment records - trigger will handle associated Loan Part Payment records
		Shift_Loan_Payment_Handler.deleteLoanPaymentList( oldLoanPaymentIdList );

		//When all Loan Payments saved then create debit/credit transactions
		if (Shift_Loan_Payment_Handler.saveLoanPaymentList( loanPaymentList ) ){
			//Create debit/credit transactions for borrowers and lenders
			Shift_Transaction_Handler.createTransactionsOnLoanClose(generateTransactionList);
		}

		//Update Loan Part functions
		Shift_Loan_Part_Handler.updateLoanPartStatus ( loanRetiredIdList, Shift_Utility.RETIRED_LOAN_PART);
		Shift_Loan_Part_Handler.updateLoanPartStatus ( loanInCollectionsIdList, Shift_Utility.IN_COLLECTIONS);
		Shift_Loan_Part_Handler.updateLoanPartStatus( loanDeadIdList, Shift_Utility.CANCELLED_PLEDGE);
	}

	/*
	* Retire a single Loan record
	* Used by - class Shift_Discharge_Loan
	*/
	public static void retireLoan(Loan__c loan){
		RecordType LoanRetireRecordType = Shift_Utility.getRecordType(Shift_Utility.RETIRED_LOAN_DEVELOPER_NAME);
		loan.Stage__c = Shift_Utility.RETIRED_LOAN;

		//Update Record Type Id
		Loan.RecordTypeId = LoanRetireRecordType.Id;

		//Update Loan Term
		Date startDate = loan.Loan_Start_Date__c;
		Date endDate = Date.today();
		Integer monthDiff = startDate.monthsBetween(endDate);

		if (endDate.day() > startDate.day()) monthDiff++;
		loan.Loan_Term__c = monthDiff;

		updateLoanList(new Loan__c[]{loan});
	}

	/*
	* Insert Loan Payments
	* Return: If there were any errors or nothing to process, return false
	*/ 
	public static void updateLoanList(list<Loan__c> loanList){
		//Only insert if the list is not empty
		if(!loanList.isEmpty()) {
      		Database.SaveResult[] result = Database.Update(loanList, false);
        	HandleInsertion_Errors(result, loanList);
    	}
	}

	/*
	* Method for handling insertion/update errors
	* If there are any errors return false
	*/
  	@TestVisible 
  	public static void HandleInsertion_Errors(Database.SaveResult[] result, list<Loan__c> resultList){
    //Counter to loop through list
    Integer counter = 0;

        for (Database.SaveResult sr : result) {
          if (sr.isSuccess()) {
              //Operation was successful, so get the ID of the record that was processed
              System.debug('#### Update/Insert Operation Successful. Object ID: ' + sr.getId());
          } else {
              //Operation failed, so get all errors
              for(Database.Error err : sr.getErrors()) {
                  System.debug(err.getStatusCode() + ': ' + err.getMessage());
              }
              resultList[counter].adderror( sr.getErrors()[0].getmessage() + ' | '+ resultList[counter].Id); 
          }
          counter++;
        }
  }
}