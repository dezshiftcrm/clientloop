/* 
* Name          : Shift_UnscheduledPaymentExtension
* Author        : Shift CRM
* Description   : Extension for Unscheduled Loan Payments Page
* Sharing Rules : With sharing modifier used to run as System for Visualforce page
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 12/15/2014         Desmond     1.0          Initial
*/

public with sharing class Shift_UnscheduledPaymentExtension {

    public Loan__c theLoan;

    //Output fields
    public Decimal principalOwed {get; set;}
    public Decimal avaiableCashBalance {get; set;}
    public String businessName {get; set;}

    //Input field(s)
    public Decimal amountPaid {get; set;}

    /*
    * Constructor
    */
    public Shift_UnscheduledPaymentExtension(ApexPages.StandardController sc) {

        //Pull all remaining Loan Payments schduled for after today and recordtype value
        theLoan = (Loan__c)sc.getRecord();
        theLoan = [SELECT Id, Outstanding_Principal__c, Total_Owed_by_Borrower__c, Business_Name__c, Business_Name__r.Name, Business_Name__r.Available_Cash_Balance__c, Interest_Rate__c, Loan_Start_Date__c FROM Loan__c WHERE Id = :theLoan.Id];

        //Set field values
        principalOwed = theLoan.Outstanding_Principal__c;
        principalOwed = principalOwed.setScale(2);
        businessName = theLoan.Business_Name__r.Name;

    }

    /*
    * Deletes the unpaid payments, and creates discharge payment.
    */ 
    public PageReference save(){

        if (amountPaid >= principalOwed){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, System.label.Excess_Payment));
            return null;
        }

        if (amountPaid > avaiableCashBalance){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, System.label.Insufficient_Funds));
            return null;
        }

        Shift_UnscheduledPayment.makeUnscheduledPayment(theLoan, amountPaid);
        
        //Redirect back to loan page
        PageReference lp = new PageReference('/'+ theLoan.Id);
        lp.setRedirect(true);
        return lp;
    }

    /*
    * Cancel action
    */
    public PageReference cancel(){
        //Redirect back to loan page
        PageReference lp = new PageReference('/'+ theLoan.Id);
        lp.setRedirect(true);
        return lp;
    }

}