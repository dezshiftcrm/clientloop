/* 
* Name          : Shift_Loan_Payment_Handler
* Author        : Shift CRM
* Description   : This class handles all calls related to Loan payment records from triggers and scheduled jobs
* Sharing Rules	: Without sharing modifier used to run as System
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 01/21/2014         Desmond     1.0         Initial
* 
*/

public Without sharing class Shift_Loan_Payment_Handler {

	/*
	* Trigger handler for Loan_Payment__c on After Insert
	* Description - Mainly used to generate Loan_Part_Payment records after a bunch of Loan_Payment records are generated.
	*/
	public static void onAfterInsert(Loan_Payment__c[] theLoanPayments){

		//Create Loan_Part_Payments
		Shift_Loan_Part_Payment_Handler.createLoanPartPaymentOnLoanClose(theLoanPayments);

	}

	/*
	* Trigger handler for Loan_Payment__c on After Delete
	*/
	public static void onAfterDelete(Loan_Payment__c[] theLoanPayments){

		//Delete corresponding Loan_Part_Payment records
		Shift_Loan_Part_Payment_Handler.deleteLoanPartPayment(theLoanPayments);

	}

	/*
	* Trigger handler for Loan_Payment__c on After Update
	*/
	public static void onAfterUpdate(Loan_Payment__c[] newLoanPayments, Map<Id,Loan_Payment__c> oldLoanPaymentMap){

		Set<Id> processIdSet = new Set<Id>();

		Loan_Payment__c oldLp;
		for (Loan_Payment__c lp: newLoanPayments){

			oldLp = oldLoanPaymentMap.get(lp.Id);

			//If there was a change in payment, process it
			if ( oldLp.Total_Paid__c == 0 && lp.Total_Paid__c > 0 ){
				processIdSet.add(lp.Id);
			}
		}

		//Exit if the process list is empty
		if (processIdSet.isEmpty()){
			return;
		}

		//Create the transactions
		Shift_Transaction_Handler.createTransaction( newLoanPayments, processIdSet );

	}

	/*
	* Generate Monthly Payment schedule based off of the amortimization method selected on the Loan
	*/ 
	public static List<Loan_Payment__c> generateMonthlyPaymentLoans(Loan__c loan, Boolean draftMode){
		
		List<Loan_Payment__c> tempList = new List<Loan_Payment__c>();

		tempList.addAll(generatePaymentSchedules(loan, draftMode));

    	return tempList;
	}

	/*
	* Generate Loan Payment records for all amortization methods.
	*/ 
	public static List<Loan_Payment__c> generatePaymentSchedules (Loan__c loan, Boolean draftMode ){

		String method = loan.Amortization_Method__c;
		//Tracking number for each Loan Payment being generated
		Integer sequenceNumber = 1;
		List<Loan_Payment__c> tempPaymentScheduleList = new List<Loan_Payment__c>();

		RecordType schedulePaymentRecordType = (draftMode) ?  [SELECT Id FROM RecordType WHERE DeveloperName = :Shift_Utility.DRAFT_PAYMENT AND SobjectType = :Shift_Utility.LOAN_PAYMENT_SOBJECT] : 
															  [SELECT Id FROM RecordType WHERE DeveloperName = :Shift_Utility.SCHEDULED_PAYMENT AND SobjectType = :Shift_Utility.LOAN_PAYMENT_SOBJECT];
		
		//General variables used by all methods
		Decimal interestOwed;
		Decimal principalOwed = loan.Loan_Amount__c / loan.Amortization_Period__c;
		Decimal monthlyInterestRate = loan.Interest_Rate__c / loan.Loan_Term__c;
		Decimal outstandingPrincipal = Loan.Loan_Amount__c;

		//Declining Balance Variables and calculations
		Decimal incrementNumber, monthlyPayment;
		Integer exponentNumber;
		if (method == Shift_Utility.DECLINING_BALANCE){
			incrementNumber = (monthlyInterestRate/100) + 1;
			exponentNumber = Integer.valueOf(loan.Amortization_Period__c);
			monthlyPayment = loan.Loan_Amount__c * ( monthlyInterestRate * (incrementNumber.pow(exponentNumber))) / (incrementNumber.pow(exponentNumber) - 1) / 100;
			monthlyPayment = monthlyPayment.setScale(2);	
		}
		
		//Interest-Only calculations
		if (method == Shift_Utility.INTEREST_ONLY){
			interestOwed = loan.Loan_Amount__c * loan.Interest_Rate__c / loan.Number_of_Scheduled_Payments__c / 100;
		}

		for (Integer x=0, j=Integer.valueOf(loan.Loan_Term__c); x < j ; x++){

			interestOwed = calcInterestOwed (outstandingPrincipal, monthlyInterestRate, interestOwed, method);
			principalOwed = calcPrincipalOwed(monthlyPayment, principalOwed, interestOwed, method);
			//Create new Loan Payments with new calculations
			tempPaymentScheduleList.add( new Loan_Payment__c (Loan__c = loan.id, 
									Principal_Owed__c = principalOwed,
									Principal_Scheduled__c = (draftMode) ? 0 : principalOwed,
                                    Principal_Paid__c = 0,
									Interest_Scheduled__c = (draftMode) ? 0 : interestOwed,
									Interest_Owed__c = interestOwed,
                                    Interest_Paid__c = 0,
                                    Fees_Owed__c = 0,
                                    Fees_Paid__c = 0,
                                    Payment_Due_Date__c = (draftMode) ? null : loan.Loan_Start_Date__c.addMonths(x+1),
									RecordTypeId = schedulePaymentRecordType.Id,
									Payment_Number_Sequence__c = sequenceNumber + x) );

			outstandingPrincipal -= principalOwed;

		}

		//For method Interest-Only last scheduled Payment Loan is charged the complete Principal owed 
		if (method == Shift_Utility.INTEREST_ONLY){
			Loan_Payment__c lastPayment = tempPaymentScheduleList.get(tempPaymentScheduleList.size() - 1);
			lastPayment.Principal_Owed__c =  loan.Loan_Amount__c;
			lastPayment.Principal_Scheduled__c = (draftMode) ? 0 : loan.Loan_Amount__c;
		}

		//For method Declining-Balance, an outstanding balance always remains, which is added to the last Loan Payment
		if (method == Shift_Utility.DECLINING_BALANCE){
			Loan_Payment__c lastPayment = tempPaymentScheduleList.get(tempPaymentScheduleList.size() - 1);
			lastPayment.Principal_Owed__c = lastPayment.Principal_Owed__c + outstandingPrincipal;
			lastPayment.Principal_Scheduled__c = (draftMode) ? 0 : lastPayment.Principal_Scheduled__c + outstandingPrincipal;
		}
		return tempPaymentScheduleList;

	}

	public static Decimal calcPrincipalOwed(Decimal monthlyPayment, Decimal principalOwed, Decimal interestOwed, String method){
		if(method == Shift_Utility.STRAIGHT_LINE){
			return principalOwed;
		} else if (method == Shift_Utility.DECLINING_BALANCE){
			return monthlyPayment - interestOwed;
		} else {
			return 0;
		}
	}

	public static Decimal calcInterestOwed (Decimal outstandingPrincipal, Decimal monthlyInterestRate, Decimal interestOwed, String method){
		if (method == Shift_Utility.INTEREST_ONLY){
			return interestOwed;
		} else { //Declining-Balance and Straight-Line
			return outstandingPrincipal * monthlyInterestRate / 100;
		}
	}

	/*
	* Delete Loan Payments
	*/
	public static void deleteLoanPaymentList(list<Id> loanIdList){

		//Gather Loan Payments related to the Loan Id list
		list<Loan_Payment__c> oldLoanPaymentList = [SELECT Id, Loan__c FROM Loan_Payment__c WHERE loan__c IN :loanIdList];

		//Only delete if the list is not empty
		if(!oldLoanPaymentList.isEmpty()) {
      		Database.DeleteResult[] result = Database.Delete(oldLoanPaymentList, false);
      		
        	Handle_Errors_Delete(result, oldLoanPaymentList);
    	}
	}

	/*
	* Insert Loan Payments
	* Return: If there were any errors or nothing to process, return false
	*/ 
	public static Boolean saveLoanPaymentList(list<Loan_Payment__c> loanPaymentList){

		for (Loan_Payment__c b : loanPaymentList){
			System.debug('Inserting: ' + b);
		}

		//Only insert if the list is not empty
		if(!loanPaymentList.isEmpty()) {
      		Database.SaveResult[] result = Database.Insert(loanPaymentList, false);
      		
        	return Handle_Errors(result, loanPaymentList);
    	}

    	return false;

	}

	/*
	* Update Loan Payments
	* If there are any errors or nothing to process, return false
	*/ 
	public static Boolean updateLoanPaymentList(list<Loan_Payment__c> loanPaymentList){

		//Only insert if the list is not empty
		if(!loanPaymentList.isEmpty()) {
      		Database.SaveResult[] result = Database.Update(loanPaymentList, false);
      		
        	return Handle_Errors(result, loanPaymentList);
    	}

    	return false;

	}

	/*
	* Method for handling insertion/update errors
	* If there are any errors return false
	*/
  	@TestVisible 
  	public static Boolean Handle_Errors(Database.SaveResult[] result, list<Loan_Payment__c> resultList){
    //Counter to loop through list
    Integer counter = 0;
    Boolean returnFlag = true;

        for (Database.SaveResult sr : result) {
          if (sr.isSuccess()) {
              //Operation was successful, so get the ID of the record that was processed
              System.debug('#### Update/Insert Operation Successful. Loan_Payment__c ID: ' + sr.getId());
          } else {
              //Operation failed, so get all errors
              for(Database.Error err : sr.getErrors()) {
                  System.debug('#### The following error has occurred.');
                  System.debug(err.getStatusCode() + ': ' + err.getMessage());
                  System.debug('#### Update/Insert Operation fields that affected this error: ' + err.getFields());
              }
              //trigger.newmap.get(resultList[counter].Loan__c).adderror( sr.getErrors()[0].getmessage() + ' | '+ resultList[counter].id);  
              resultList[counter].adderror( sr.getErrors()[0].getmessage() + ' | '+ resultList[counter].Id); 
              returnFlag = false;               
          }
          counter++;  
        }
        return returnFlag;
  }

	/*
	* Method for handling deletion errors
	*/ 
  	@TestVisible 
  	public static void Handle_Errors_Delete(Database.DeleteResult[] result, list<Loan_Payment__c> resultList){
    //Counter to loop through list
    Integer counter = 0;

        for (Database.DeleteResult sr : result) {
          if (sr.isSuccess()) {
              //Operation was successful, so get the ID of the record that was processed
              System.debug('#### Delete Operation Successful. Object ID: ' + sr.getId());
          } else {
              //Operation failed, so get all errors
              for(Database.Error err : sr.getErrors()) {
                  System.debug('#### The following error has occurred.');                    
                  System.debug(err.getStatusCode() + ': ' + err.getMessage());
                  System.debug('#### Delete Operation fields that affected this error: ' + err.getFields());
              }
              trigger.newmap.get(resultList[counter].Loan__c).adderror( sr.getErrors()[0].getmessage() + ' | '+ resultList[counter].id);
               //resultList[counter].adderror( sr.getErrors()[0].getmessage() + ' | '+ resultList[counter].Id);                  
          }
          counter++;  
        }
  }
}