/* 
* Name          : Shift_AdjustLoanPaymentLines
* Author        : Shift CRM
* Description   : Adjust scheduled Loan payments when unscheduled payments are made such as 
				  fully discharging a Loan (completely paying off ones Loan or making an early unscheduled payment).
				  Loan payments will either be completely paid off or their principalOwed will decrease.
* Sharing Rules : Without sharing modifier used to run as System
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 12/18/2014         Desmond     1.0          Initial
*/

public without sharing class Shift_AdjustLoanPaymentLines {

	/*
	* dischargeRemainingPaymentSchedules: Remove the remaining Loan Payments
	*/ 
	public static void dischargeRemainingPaymentSchedules(Loan__c loan){
		//Pull all remaining Loan Payments schduled for after today and recordtype value
		List<Loan_Payment__c> paymentScheduleList = [SELECT Id, Loan__c, Principal_Owed__c, Interest_Owed__c, Payment_Number_Sequence__c 
													 FROM Loan_Payment__c 
													 WHERE Loan__c = :loan.Id 
													 AND Payment_Status__c = :Shift_Utility.SCHEDULED 
													 ORDER By Payment_Number_Sequence__c ];
		deleteLoanPaymentList(paymentScheduleList);
	}

	/*
	* Straight-line payment: Re-calculate the remaining scheduled Loan Payments based off of the remaining balance.
	* Scheduled values for Loan Payment records are never updated.
	*/ 
	public static void adjustPaymentSchedule(Loan__c loan){
		
		String method = loan.Amortization_Method__c;

		//Pull all remaining Loan Payments schduled for after today and recordtype value
		RecordType scheduledPaymentRecordType = Shift_Utility.getRecordType(Shift_Utility.SCHEDULED_PAYMENT);
		
		List<Loan_Payment__c> paymentScheduleList = [SELECT Id, Principal_Owed__c, Interest_Owed__c, Payment_Number_Sequence__c 
													FROM Loan_Payment__c 
													WHERE Loan__c = :loan.Id AND Payment_Status__c = :Shift_Utility.SCHEDULED 
													AND RecordTypeId = :scheduledPaymentRecordType.Id 
													AND Payment_Due_Date__c >= LAST_N_DAYS:7 
													ORDER By Payment_Number_Sequence__c ];

		//General variables used by all methods
		Decimal interestOwed;
		Decimal principalOwed = loan.Outstanding_Principal__c / paymentScheduleList.size();
		Decimal monthlyInterestRate = loan.Interest_Rate__c / loan.Loan_Term__c;
		Decimal outstandingPrincipal = Loan.Outstanding_Principal__c;

		//Declining-Balance Variables and calculations
		Decimal incrementNumber, monthlyPayment;
		Integer exponentNumber;
		if (method == Shift_Utility.DECLINING_BALANCE){
			incrementNumber = (monthlyInterestRate/100) + 1;
			exponentNumber = Integer.valueOf(paymentScheduleList.size());
			monthlyPayment = loan.Outstanding_Principal__c * ( monthlyInterestRate * (incrementNumber.pow(exponentNumber))) / (incrementNumber.pow(exponentNumber) - 1) / 100;
			monthlyPayment = monthlyPayment.setScale(2);	
		}

		//Interest-Only calculations
		if (method == Shift_Utility.INTEREST_ONLY){
			interestOwed = loan.Outstanding_Principal__c * loan.Interest_Rate__c / paymentScheduleList.size() / 100;
		}

		for (Loan_Payment__c currentLoanPayment : paymentScheduleList){
			interestOwed = calcInterestOwed (outstandingPrincipal, monthlyInterestRate, interestOwed, method);
			principalOwed = calcPrincipalOwed(monthlyPayment, principalOwed, interestOwed, method);

			//Calculate interest for each Loan Payment
			currentLoanPayment.Principal_Scheduled__c = principalOwed;
			currentLoanPayment.Principal_Owed__c = principalOwed;
			currentLoanPayment.Interest_Scheduled__c = interestOwed;
			currentLoanPayment.Interest_Owed__c = interestOwed;

			outstandingPrincipal -= principalOwed;
		}

		if (method == Shift_Utility.DECLINING_BALANCE){
			//After the payment scheduled is calculated an outstanding balance always remains for this method, which is added to the last Loan Payment
			Loan_Payment__c lastPayment = paymentScheduleList.get(paymentScheduleList.size() - 1);
			lastPayment.Principal_Owed__c = lastPayment.Principal_Owed__c + outstandingPrincipal;
		}

		if (method == Shift_Utility.INTEREST_ONLY){
			//After the payment schedule is caculated the entirety of the Principal must be paid back and this is added to the last scheduled Payment Loan
			Loan_Payment__c lastPayment = paymentScheduleList.get(paymentScheduleList.size() - 1);
			lastPayment.Principal_Owed__c =  loan.Outstanding_Principal__c;
		}

		updateLoanPaymentList( paymentScheduleList );
	}	

	public static Decimal calcPrincipalOwed(Decimal monthlyPayment, Decimal principalOwed, Decimal interestOwed, String method){
		if(method == Shift_Utility.STRAIGHT_LINE){
			return principalOwed;
		} else if (method == Shift_Utility.DECLINING_BALANCE){
			return monthlyPayment - interestOwed;
		} else {
			return 0;
		}
	}

	public static Decimal calcInterestOwed (Decimal outstandingPrincipal, Decimal monthlyInterestRate, Decimal interestOwed, String method){
		if (method == Shift_Utility.INTEREST_ONLY){
			return interestOwed;
		} else { //Declining-Balance and Straight-Line
			return outstandingPrincipal * monthlyInterestRate / 100;
		}
	}

	/*
	* Save Loan Payments, while respecting errors and rolling them up to thier parent Loan record
	*/ 
	public static void insertLoanPaymentList (list<Loan_Payment__c> loanPaymentList){
		
		if(!loanPaymentList.isEmpty()) {
      		Database.SaveResult[] result = Database.Insert(loanPaymentList, false);	
        	Handle_Errors(result, loanPaymentList);
        }

	}

	/*
	* Delete Loan Payments, while respecting errors and rolling them up to thier parent Loan record
	*/ 
	public static void deleteLoanPaymentList (list<Loan_Payment__c> loanPaymentList){
		
		if(!loanPaymentList.isEmpty()) {
      		Database.DeleteResult[] result = Database.Delete(loanPaymentList, false);	
        	HandleDelete_Errors(result, loanPaymentList);
        }

	}

	/*
	* Save Loan Payments, while respecting errors and rolling them up to thier parent Loan record
	*/ 
	public static void updateLoanPaymentList (list<Loan_Payment__c> loanPaymentList){
		
		if(!loanPaymentList.isEmpty()) {
      		Database.SaveResult[] result = Database.Update(loanPaymentList, false);	
        	Handle_Errors(result, loanPaymentList);
        }

	}

	/*
	* Method for handling insertion/update errors
	*/
	@TestVisible
	private static void Handle_Errors(Database.SaveResult[] result, list<Loan_Payment__c> resultList){
	    //Counter to loop through list
	    Integer counter = 0;

        for (Database.SaveResult sr : result) {
          if (sr.isSuccess()) {
              //Operation was successful, so get the ID of the record that was processed
              System.debug('#### Update/Insert Operation Successful. Object ID: ' + sr.getId());
          } else {
              //Operation failed, so get all errors
              for(Database.Error err : sr.getErrors()) {
                  System.debug(err.getStatusCode() + ': ' + err.getMessage());
                  System.debug('#### Update/Insert Operation fields that affected this error: ' + err.getFields());
              }
              resultList[counter].adderror( sr.getErrors()[0].getmessage() + ' | '+ resultList[counter].Id);                
          }
          counter++;  
        }
  	}

	/*
	* Method for handling delete errors
	*/
	@TestVisible
	private static void HandleDelete_Errors(Database.DeleteResult[] result, list<Loan_Payment__c> resultList){
	//Counter to loop through list
	Integer counter = 0;

	for (Database.DeleteResult sr : result) {
		if (sr.isSuccess()) {
			//Operation was successful, so get the ID of the record that was processed
			System.debug('#### Delete Operation Successful. Object ID: ' + sr.getId());
		} else {
			//Operation failed, so get all errors
			for(Database.Error err : sr.getErrors()) {
				System.debug('#### The following error has occurred.');
				System.debug(err.getStatusCode() + ': ' + err.getMessage());
				System.debug('#### Update/Insert Operation fields that affected this error: ' + err.getFields());
			}
				resultList[counter].adderror( sr.getErrors()[0].getmessage() + ' | '+ resultList[counter].Id);                
			}
			counter++;  
		}
	}
}