/* 
* Name          : Shift_ExtendLoanTerm
* Author        : Shift CRM
* Description   : Extends a loans term from a button click
* Sharing Rules : With sharing modifier used to run as User
*
* Maintenance History: 
* Date (DD/MM/YYYY)- Name  ----  Version ---  Remarks 
* 12/18/2014         Desmond     1.0          Initial
* 06/02/2015         Desmond     1.1          Code clean up
*/

public with sharing class Shift_ExtendLoanTerm {
    
    /*
    * extendTerm - creates additional Loan Payment records and updates their relevant field values.
    */ 
    public static void extendTerm(Loan__c loan, Integer newTerm, Integer newPeriod) {

        Loan__c theLoan;
        try {
            //Query all necessary fields for processing Loan
            theLoan = [SELECT Id, Outstanding_Principal__c, Loan_Amount__c, Amortization_Method__c, Amortization_Period__c, Interest_Rate__c, Loan_Term__c, Loan_Start_Date__c, Initial_Loan_Term__c 
                       FROM Loan__c WHERE Id = :loan.Id];   
        } catch (QueryException e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.label.Query_Error + ' | ' + e));
            System.debug(System.label.Query_Error + ' Error on record: ' + loan.Id);
            return;
        }

        //Calculate loan term changes and update the Loan
        Integer numberOfNewTerms = newTerm - theLoan.Loan_Term__c.intValue();
        Loan.Initial_Loan_Term__c = theLoan.Loan_Term__c;
        theLoan.Loan_Term__c = newTerm;
        theLoan.Amortization_Period__c = newPeriod;
        Shift_Loan_Handler.updateLoanList(new Loan__c[]{theLoan});
        Date paymentStartDate = theLoan.Loan_Start_Date__c.addMonths( theLoan.Initial_Loan_Term__c.intValue() ) ;
        
        //Get record type Id for Loan with developer name scheduled_payment
        RecordType schedulePaymentRecordType = Shift_Utility.getRecordType(Shift_Utility.SCHEDULED_PAYMENT);
        
        //If recordtype cannot be found
        if (schedulePaymentRecordType == null){ 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.label.Query_Error));
        }
        
        Integer sequenceNumber = theLoan.Initial_Loan_Term__c.intValue() + 1;
        list<Loan_Payment__c> newLoanPaymentList = new List<Loan_Payment__c>();

        //Create the additional Loan Payment records
        for (Integer x = 0; x < numberOfNewTerms; x++){
            newLoanPaymentList.add( new Loan_Payment__c (Loan__c = theLoan.id, 
                                    Principal_Owed__c = 0,
                                    Principal_Scheduled__c = 0,
                                    Principal_Paid__c = 0,
                                    Interest_Scheduled__c = 0,
                                    Interest_Owed__c = 0,
                                    Interest_Paid__c = 0,
                                    Fees_Owed__c = 0,
                                    Fees_Paid__c = 0,
                                    Payment_Due_Date__c = paymentStartDate.addMonths(x+1),
                                    RecordTypeId = schedulePaymentRecordType.Id,
                                    Payment_Number_Sequence__c = sequenceNumber + x)
                                  );
        }

        //Insert new Loan Payment records and adjust field values for the new term(s)
        Shift_AdjustLoanPaymentLines.insertLoanPaymentList(newLoanPaymentList);
        Shift_AdjustLoanPaymentLines.adjustPaymentSchedule(theLoan);
    }
}