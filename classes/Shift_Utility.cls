/* 
* Name          : Shift_Utility
* Author        : Shift CRM
* Description   : Class for storing static variables - should this be moved to Custom Settings?
* Sharing Rules : Without sharing modifier used to run as System
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 10/12/2014         Desmond     1.0          Initial
* 06/02/2015         Desmond     1.1          Record Type Functionality
*/


public without sharing class Shift_Utility {

	//Amoritization Methods
	final public static String STRAIGHT_LINE = 'Straight-Line';
	final public static String DECLINING_BALANCE = 'Declining Balance';
	final public static String INTEREST_ONLY = 'Interest Only Payments';
	
	//Loan Stages
	final public static String FUNDED_AWAITING_ACCEPTANCE = 'Funded - Awaiting Acceptance';
	final public static String CLOSED = 'Closed';
	final public static String APPROVED_FINALIZING_DETAILS = 'Approved - Finalizing Details';
	final public static String DEAD_LOAN = 'Dead Loan';
	final public static String CANCELLED_PLEDGE = 'Cancelled Pledge';
	final public static String IN_COLLECTIONS = 'In Collections';
	final public static String RETIRED_LOAN = 'Retired Loan';
	final public static String PENDING_LOAN_START = 'Pending Loan Start';
	final public static String LISTED_LOAN = 'Listed Loan';

	//Loan Dead Reasons
	final public static String DEAD_REASON_NOT_FUNDED = 'Not Funded During Listing';
	final public static String DEAD_REASON_DECLINED = 'Application Declined';
	final public static String DEAD_REASON_CANCELLED = 'Cancelled Loan';

	//Loan Part Stages
	final public static String ACTIVE_LOAN_PART = 'Active Loan Part';
	final public static String PLEDGE_LOAN_PART = 'Pledge';
	final public static String RETIRED_LOAN_PART = 'Retired Loan Part';
	final public static String CANCELLED_PLEDGE_LOAN_PART = 'Cancelled Pledge';
	final public static String CANCELLED_LOAN_PART = 'Cancelled Loan';
	final public static String IN_COLLECTIONS_LOAN_PART = 'In Collections';

	//Record Type Developer Name
	final public static String BORROWER_DEVELOPER_NAME = 'Borrower';
	final public static String LENDER_DEVELOPER_NAME = 'Lender';
	final public static String DRAFT_PAYMENT = 'Draft_Payment';
	final public static String SCHEDULED_PAYMENT = 'Scheduled_Payment';
	final public static String UNSCHEDULED_PAYMENT = 'Principal_Repayment';
	final public static String DISCHARGE_PAYMENT = 'Discharge_Payment';
	final public static String RETIRED_LOAN_DEVELOPER_NAME = 'Retired_Loan';
	final public static String PENDING_LISTING_DEVELOPER_NAME = 'Pending_Listing';
	final public static String LISTING_LOAN_DEVELOPER_NAME = 'Listed_Loan';
	final public static String FUNDED_AWAITING_DEVELOPER_NAME = 'Funded_Awaiting_Acceptance';
	final public static String CLOSED_LOAN_DEVELOPER_NAME = 'Closed_Loan';
	final public static String DEAD_LOAN_DEVELOPER_NAME = 'Dead_Loan';
	final public static String CREDIT_INTERNAL_DEVELOPER_NAME = 'Credit_Internal';
	final public static String ACTIVE_DEVELOPER_NAME = 'Active';

	final public static String CREDIT_EXTERNAL = 'Credit (External)';
	final public static String CREDIT_INTERNAL = 'Credit (Internal)';
	final public static String DEBIT_EXTERNAL = 'Debit (External)';
	final public static String DEBIT_INTERNAL = 'Debit (Internal)';

	//Object Type
	final public static String LOAN_PAYMENT_SOBJECT = 'Loan_Payment__c';
	final public static String LOAN_SOBJECT = 'Loan__c';
	final public static String TRANSACTION_SOBJECT = 'Transaction__c';
	final public static String ACCOUNT_SOBJECT = 'Account';
	//Loan Payment Status
	final public static String SCHEDULED = 'Scheduled';

	//Date
	final public static Integer DAYS_IN_YEAR = 365;

	//Record Type Map
	private static Map<String, RecordType> recordTypeMap;

	//Record Type
	public static RecordType getRecordType(String recordTypeName){

		if (recordTypeMap == null){
			recordTypeMap = new Map<String, RecordType>();
			try {
				List<RecordType> recordTypeList = [SELECT Id, Name, DeveloperName FROM RecordType];
				for (RecordType r : recordTypeList){
					recordTypeMap.put(r.DeveloperName, r);
				}
			} catch(QueryException e){
				System.debug(System.label.Query_Error + ' Error: ' + e);
			}
		}
		
		RecordType theRecordType = recordTypeMap.get(recordTypeName);
		return theRecordType;
	}

	//Converts file size in bytes into human readable format
	public static String humanReadableByteCount(long bytes) {
		Integer unit = 1000;
		if (bytes < unit) return bytes + ' B';
		Integer exp = (Integer) (Math.log(bytes) / Math.log(unit));
		String units = 'kMGTPE';
		String chosenUnit = units.substring(exp-1, exp);
		Decimal byteValue = bytes / Math.pow(unit, exp);
		return String.valueOf(byteValue.setScale(1)) + ' ' + chosenUnit + 'B'; 
		//return String.format('%.1f %sB', bytes / Math.pow(unit, exp), pre);
	}

}