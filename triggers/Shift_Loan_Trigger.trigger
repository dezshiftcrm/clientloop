/* 
* Name          : Shift_Loan_Trigger
* Author        : Shift CRM
* Description   : Manages firing of triggered events on Loan__c
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 12/08/2014         Desmond     1.0          Initial
*/

trigger Shift_Loan_Trigger on Loan__c (after update) {

    if (Trigger.isUpdate && Trigger.isAfter){
        Shift_Loan_Handler.onAfterUpdate(Trigger.new, Trigger.oldMap);
    }

}