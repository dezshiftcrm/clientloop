//Description - Mainly used to generate Loan_Part_Payment records after a bunch of Loan_Payment records are generated.

trigger Shift_Loan_Payment_Trigger on Loan_Payment__c (after insert, after update) {


	if (Trigger.isInsert && Trigger.isAfter){
		Shift_Loan_Payment_Handler.onAfterInsert(Trigger.new);
	}

	if (Trigger.isUpdate && Trigger.isAfter){
		Shift_Loan_Payment_Handler.onAfterUpdate(Trigger.new, Trigger.oldMap);	
	}

}