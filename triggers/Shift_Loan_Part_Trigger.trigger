trigger Shift_Loan_Part_Trigger on Loan_Part__c (after insert) {

	if (Trigger.isInsert && Trigger.isAfter){
		Shift_Loan_Part_Handler.onAfterInsert();
	}

}